﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;


namespace DataGunamagari.Models
{
    public static class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }
            if(context.Schedules.Any())
            {
                return;
            }
            var l1 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var l2 = context.Locations.Add(new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" });
            var l3 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var l4 = context.Locations.Add(new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" });
            var l5 = context.Locations.Add(new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" });
            var l6 = context.Locations.Add(new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" });

            context.Schedules.AddRange(
                new Schedule() { StartLocation = "CH",EndLocation="Vk",VehicleNumber=1, LocationID = l1.Entity.LocationID},
                new Schedule() { StartLocation = "GS", EndLocation = "CH", VehicleNumber = 2, LocationID = l1.Entity.LocationID },
                new Schedule() { StartLocation = "GS", EndLocation = "LIB", VehicleNumber = 3, LocationID = l2.Entity.LocationID },
                new Schedule() { StartLocation = "CH", EndLocation = "SU", VehicleNumber = 4, LocationID = l3.Entity.LocationID },
                new Schedule() { StartLocation = "SU", EndLocation = "LIB", VehicleNumber = 5, LocationID = l4.Entity.LocationID },
                new Schedule() { StartLocation = "ARTS", EndLocation = "SU", VehicleNumber = 6, LocationID = l5.Entity.LocationID },
                new Schedule() { StartLocation = "Vk", EndLocation = "SU", VehicleNumber = 7, LocationID = l6.Entity.LocationID }


             );
            context.SaveChanges();

        }
    } }
