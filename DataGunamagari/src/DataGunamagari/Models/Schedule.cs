﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataGunamagari.Models
{
    public class Schedule
    {
        [ScaffoldColumn(false)]
        public int ScheduleID { get; set; }

        [Required]
        [Display(Name = "Starting Location")]
        public String StartLocation { get; set; }

        [Display(Name = "Ending Location")]
        public string EndLocation { get; set; }

      /*  [Display(Name = "Start Time")]
        public string StartTime { get; set; }

        [Required]
        [Display(Name = "End Time")]
        public string EndTime { get; set; }

        [Required]
        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }
        */
        [Required]
        [Range(0000, 2359)]
        [Display(Name = "Vehicle Number")]
        public int VehicleNumber { get; set; }

       // [Required]
     //   [RegularExpression(@"^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$")]
     //   [Display(Name = "Driver Contact Number")]
     //   public int contactnumber { get; set; }//*/

//
public int LocationID { get; set; }
        public virtual Location Location { get; set; }
    }
}

